#!/usr/bin/env python3

"""
    Script for analysis of stresses and deformations of a space frame truss.
    Copyright (C) 2014 - Diego Cucco.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

# Name and version of the script
NAME = "Space Frame Truss"
VERSION = "0.1"
# ----

import transformation as tr  # contains the transformation functions
import tkinter as tk
import tkinter.ttk as ttk

# Define some points an beams for test.
# Points.
P = [
    ["p1", 100, 100, 0],
    ["p2", 200, 100, 0],
    ["p3", 200, 200, 0],
    ["p4", 100, 200, 0],
    ["p5", 100, 100, 100],
    ["p6", 200, 100, 100],
    ["p7", 200, 200, 100],
    ["p8", 100, 200, 100]
    ]

# Beams.
A = [
    ["a1", "p1", "p2"],
    ["a2", "p2", "p3"],
    ["a3", "p3", "p4"],
    ["a4", "p4", "p1"],
    ["a5", "p1", "p5"],
    ["a6", "p5", "p6"],
    ["a7", "p2", "p6"],
    ["a8", "p6", "p7"],
    ["a9", "p7", "p8"],
    ["a10", "p5", "p8"],
    ["a11", "p4", "p8"],
    ["a12", "p3", "p7"]
    ]
# ----

# Default property of canvas.
WIDTH = 500
HEIGHT = 500
BG = "#FDF6E3"
# ----

# Default coordinates of the new point around which make rotation
# actually it's a point on the monitor so, it will be ever Z = 0.
O = [WIDTH/2, HEIGHT/2, 0]
# ----

# Default angles of rotation.
AX = 30
AY = 30
AZ = 30
# ----

# Texts.
COLUMN = ["Select",
          "Beam", "From point", "To Point", "|",
          "Point", "X", "Y", "Z"]

TAB = ["Beams & Points", "TO DO"]

ROTATION = ["Rotate X", "Rotate Y", "Rotate Z"]

ORIGIN = "Rotate around"

DRAW = "Draw"
# ----


class Gui(tk.Frame):
    """
    Defines all the widgets.
    """

    def __init__(self, master=None):
        """
        Initializes the main frames:
        there are three vertical main frames (from left to right):
        1) frame for transformation widgets;
        2) frame for canvas;
        3) notebook for values and results (each its tab is a frame).
        """
        tk.Frame.__init__(self, master)
        self.grid()

        # Main frames of the window.
        self.t = tk.Frame(master)
        self.t.grid(row=0, column=0)

        self.d = tk.Frame(master)
        self.d.grid(row=0, column=1)

        n = ttk.Notebook(master)
        n.grid(sticky=tk.N+tk.E+tk.S+tk.W, row=0, column=2)
        # ----

        # Frames for notebook.
        self.f_tab1 = tk.Frame(n)
        self.f_tab1.grid(row=0, column=0)

        self.f_tab2 = tk.Frame(n)
        self.f_tab2.grid(row=0, column=0)

        n.add(self.f_tab1, text=TAB[0])
        n.add(self.f_tab2, text=TAB[1])
        # ----

        # Call the functions which create the widgets.
        self.create_button_rotation()
        self.create_button_origin()
        self.create_canvas()
        self.create_entry_draw()
        self.create_checkbox()
        self.create_()
        self.create_separator()
        self.create_entry_point()
        self.write_value()
        #----

    def create_button_rotation(self):
        """
        Creates the buttons for rotation around X, Y, Z axes.
        """
        bx = tk.Button(self.t, text=ROTATION[0], command=self.rotate_x)
        bx.grid(sticky=tk.E+tk.W, row=0, column=0)

        by = tk.Button(self.t, text=ROTATION[1], command=self.rotate_y)
        by.grid(sticky=tk.E+tk.W, row=1, column=0)

        bz = tk.Button(self.t, text=ROTATION[2], command=self.rotate_z)
        bz.grid(sticky=tk.E+tk.W, row=2, column=0)

    def create_button_origin(self):
        """
        Creates the button for choose a new rotation origin.
        """
        b = tk.Button(self.t, text=ORIGIN, command=self.new_origin)
        b.grid(sticky=tk.E+tk.W, row=3, column=0)

    def create_canvas(self):
        """
        Creates the canvas where draw points, beams and other.
        """
        self.c = tk.Canvas(self.d, bg=BG, width=WIDTH, height=HEIGHT)
        self.c.grid(row=0, column=0, columnspan=3)

    def create_entry_draw(self):
        """
        Creates the button for draw and the entry fields where insert
        the X, Y, Z coordinates of the two extremes of a beam.

        The entry fields are initialized with examples values in order to
        show the correct syntax: X Y Z rather than X,Y,Z or some thing else.
        """
        self.v1 = tk.DoubleVar()
        self.v1.set("X1 Y1 Z1")

        self.v2 = tk.DoubleVar()
        self.v2.set("X2 Y2 Z2")

        self.e1 = tk.Entry(self.d, textvariable=self.v1)
        self.e1.grid(row=1, column=0)

        self.e2 = tk.Entry(self.d, textvariable=self.v2)
        self.e2.grid(row=1, column=1)

        b = tk.Button(self.d, text=DRAW, command=self.draw)
        b.grid(row=1, column=2)

    def create_checkbox(self):
        """
        Creates as many checkboxes as beams are.
        """
        columns = COLUMN[0]
        l = tk.Label(self.f_tab1, text=columns)
        l.grid(row=0, column=0)

        # Store here the name of the checkboxes widgets.
        # Each name matching the number of the row of the matrix of beams.
        self.entry_check = []  # [["r"], ..]

        for r in range(len(A)):  # r = ["ar", "p0", "p1"]
            pos = str(r)  # names of widgets
            self.entry_check.append(pos)

            pos = tk.Checkbutton(self.f_tab1)
            pos.grid(row=r+1, column=0)

    def create_(self):
        """
        Creates as many entries as beams are.
        """
        columns = COLUMN[1:4]
        for c in range(len(columns)):
            l = tk.Label(self.f_tab1, text=columns[c])
            l.grid(row=0, column=c+1)

        # Store here the name and the text variable of the entry widgtes.
        # Each name is a concatenation of the number of the row and the
        # number of the column of the matrix of beams.
        self.entry_beam = []  # [["r,c", var], ..]

        for r in range(len(A)):  # r = ["ai", "p0", "p1"]
            for c in range(len(A[r])):  # c = 0, .., 2
                pos = str(r) + "," + str(c)  # names of widgets
                var = tk.StringVar()
                self.entry_beam.append([pos, var])

                pos = tk.Entry(self.f_tab1, textvariable=var, width=10)
                pos.grid(row=r+1, column=c+1)

    def create_separator(self):
        """
        Creates a vertical separator.
        """
        r = len(A)
        s = ttk.Separator(self.f_tab1, orient=tk.VERTICAL)
        s.grid(row=0, rowspan=r+1, column=4, sticky=tk.N+tk.S)

    def create_entry_point(self):
        """
        Creates as many entries as beams are.
        """
        columns = COLUMN[5:]
        for c in range(len(columns)):
            l = tk.Label(self.f_tab1, text=columns[c])
            l.grid(row=0, column=c+5)

        # Store here the name and the text variable of the entry widgtes.
        # Each name is a concatenation of the number of the row and the
        # number of the column of the matrix of points.
        self.entry_point = []  # [["r,c", var], ..]

        for r in range(len(P)):  # r = ["pi", x, y, z, "ai"]
            for c in range(len(P[r])):  # c = 0, .., 4
                pos = str(r) + "," + str(c)  # nomi widget
                var = tk.StringVar()
                self.entry_point.append([pos, var])

                pos = tk.Entry(self.f_tab1, textvariable=var, width=10)
                pos.grid(row=r+1, column=c+5)

    def write_value(self):
        """
        Populates each entry field with its value.
        """
        for i in iter(self.entry_point):  # i = ["r,c", var]
            rc = i[0].split(",")  # rc = ["r", "c"]
            r = int(rc[0])  # number of row
            c = int(rc[1])  # number of column
            i[1].set(P[r][c])

        for i in iter(self.entry_beam):  # i = ["r,c", var]
            rc = i[0].split(",")  # rc = ["r", "c"]
            r = int(rc[0])  # numero riga
            c = int(rc[1])  # numero colonna
            i[1].set(A[r][c])


class Make(Gui, tr.Tr):
    """
    Defines all the functions that make some transformations on the canvas.
    """

    def draw(self):
        """
        Given two points, it draws a line.

        THIS FUNCTION NEEDS TO BE FIXED!!
        """
        for i in iter(A):  # i = ["ai", "p0", "p1"]
            flag_a0 = 0
            flag_a1 = 0
            # Get the names of the points from the extrems of the line.
            a0 = i[1]
            a1 = i[2]

            for j in iter(P):  # j = ["pj", x, y, z]
                # Get the X and Y value from the extremes of the line.
                if flag_a0 == 0:
                    if (a0 in j) is True:
                        a0 = P.index(j)
                        flag_a0 = 1

                if flag_a1 == 0:
                    if (a1 in j) is True:
                        a1 = P.index(j)
                        flag_a1 = 1

                if flag_a0 == 1 and flag_a1 == 1:
                    break  # stop search and break cycle for j

            # Draw a line.
            self.c.create_line(P[a0][1], P[a0][2],  # x0, y0
                               P[a1][1], P[a1][2],  # x1, y1
                               tags=i[0])  # ai

            flag_a0 = 0
            flag_a1 = 0

    def redraw(self):
        """
        Moves each line after a transformation.
        """
        for i in iter(A):  # i = ["ai", "p0", "p1"]
            flag_a0 = 0
            flag_a1 = 0
            # Get the names of the points from the extrems of the line.
            a0 = i[1]
            a1 = i[2]

            for j in iter(P):  # j = ["pj", x, y, z]
                # Get the X and Y value from the extremes of the line.
                if flag_a0 == 0:
                    if (a0 in j) is True:
                        a0 = P.index(j)
                        flag_a0 = 1

                if flag_a1 == 0:
                    if (a1 in j) is True:
                        a1 = P.index(j)
                        flag_a1 = 1

                if flag_a0 == 1 and flag_a1 == 1:
                    break  # stop search and break cycle for j

            # Moves the line changing the coordinates of its extremes.
            self.c.coords(i[0],
                          P[a0][1], P[a0][2],  # x0, y0
                          P[a1][1], P[a1][2]  # x1, y1
                          )

            flag_a0 = 0
            flag_a1 = 0

    def rotate_x(self):
        """
        Gives all the points to the function which makes a rotation around
        X axes.
        """
        for i in iter(P):  # i = ["pi", x, y, z]
            r = i[1:]  # q = [x, y, z] doesn't rotate
            q = tr.rxl(AX, r, O)  # q = [x*, y*, z*] rotate
            i[1:] = q  # [x, y, z] = [x*, y*, z*] sobstitutes new coordinates

        self.redraw()

    def rotate_y(self):
        """
        Gives all the points to the function which makes a rotation around
        Y axes.
        """
        for i in iter(P):  # i = ["pi", x, y, z]
            r = i[1:]  # q = [x, y, z] doesn't rotate
            q = tr.ryl(AY, r, O)  # q = [x*, y*, z*] rotate
            i[1:] = q  # [x, y, z] = [x*, y*, z*] sobstitutes new coordinates

        self.redraw()

    def rotate_z(self):
        """
        Gives all the points to the function which makes a rotation around
        Z axes.
        """
        for i in iter(P):  # i = ["pi", x, y, z]
            r = i[1:]  # q = [x, y, z] doesn't rotate
            q = tr.rzl(AZ, r, O)  # q = [x*, y*, z*] rotate
            i[1:] = q  # [x, y, z] = [x*, y*, z*] sobstitutes new coordinates

        self.redraw()

    def new_origin(self):
        """
        Creates an event associated with the pressure of left button of the
        mouse on the canvas.
        """
        self.c.delete("origin")
        self.c.config(cursor="cross")
        self.c.bind("<Button-1>", self.take_pos_origin)

    def take_pos_origin(self, event):
        """
        Given an event it takes the X and Y coordinates of the mouse on
        the canvas.
        """
        r = 2
        self.c.create_oval(event.x-r, event.y-r, event.x+r, event.y+r,
                           fill="red", tag="origin")
        x = self.c.canvasx(event.x)
        y = self.c.canvasy(event.y)
        self.c.config(cursor="")

        # Sobstitute the default coordinates with the new ones.
        O[0] = x
        O[1] = y

        self.c.unbind("<Button-1>")  # unbind the event with the left button


if __name__ == '__main__':
    root = tk.Tk()
    tr = tr.Tr()

    make = Make(master=root)
    make.master.title(NAME + " " + VERSION)
    make.mainloop()
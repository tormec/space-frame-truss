#!/usr/bin/env python3

"""
Script for calculate gemetric transformation.
Copyright (C) 2014 - Diego Cucco.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import math as m


class Tr(object):
    """Defines the transformation functions."""

    def dot_product(self, v, w):
        """
        Calculates the dot product of two space vectors
        """
        s = 0

        for i in range(3):
            s = round(s + (v[i] * w[i]), 2)

        return s

    def rxl(self, a, p, o):
        """
        Transforms the coordinates of a point with a counterclockwise rotation
        around X axes.

        If the origin isn't the [0, 0, 0] (actually this is a point on the
        screen, so we use only X and Y), the function:
        1) translates the point so that the new origin match the old one;
        2) rotates the point;
        3) translates the point so that the new origin match its real position.
        """
        a = m.radians(a)
        q = [0, 0, 0]

        r = [
            [1, 0, 0],  # 1 row
            [0, m.cos(a), -m.sin(a)],  # 2 row
            [0, m.sin(a), m.cos(a)]  # 3 row
            ]

        # Moves the point close to [0, 0] about a quantity equals to distance
        # between the new origin and the old one.
        p[0] = p[0] - o[0]
        p[1] = p[1] - o[1]

        # Rotate the point.
        for i in range(3):  # i = 0, .., 2
            q[i] = self.dot_product(r[i], p)

        # Moves the point away from [0, 0] so that the new origin match its
        # real coordinates.
        q[0] = q[0] + o[0]
        q[1] = q[1] + o[1]

        return q

    def ryl(self, a, p, o):
        """
        Transforms the coordinates of a point with a counterclockwise rotation
        around Y axes.

        If the origin isn't the [0, 0, 0] (actually this is a point on the
        screen, so we use only X and Y), the function:
        1) translates the point so that the new origin match the old one;
        2) rotates the point;
        3) translates the point so that the new origin match its real position.
        """
        a = m.radians(a)
        q = [0, 0, 0]

        r = [
            [m.cos(a), 0, -m.sin(a)],  # 1 row
            [0, 1, 0],  # 2 row
            [m.sin(a), 0, m.cos(a)]  # 3 row
            ]

        # Moves the point close to [0, 0] about a quantity equals to distance
        # between the new origin and the old one.
        p[0] = p[0] - o[0]
        p[1] = p[1] - o[1]

        # Rotate the point.
        for i in range(3):  # i = 0, .., 2
            q[i] = self.dot_product(r[i], p)

        # Moves the point away from [0, 0] so that the new origin match its
        # real coordinates.
        q[0] = q[0] + o[0]
        q[1] = q[1] + o[1]

        return q

    def rzl(self, a, p, o):
        """
        Transforms the coordinates of a point with a counterclockwise rotation
        around Y axes.

        If the origin isn't the [0, 0, 0] (actually this is a point on the
        screen, so we use only X and Y), the function:
        1) translates the point so that the new origin match the old one;
        2) rotates the point;
        3) translates the point so that the new origin match its real position.
        """
        a = m.radians(a)
        q = [0, 0, 0]

        r = [
            [m.cos(a), -m.sin(a), 0],  # 1 riga
            [m.sin(a), m.cos(a), 0],  # 2 riga
            [0, 0, 1]  # 3 riga
            ]

        # Moves the point close to [0, 0] about a quantity equals to distance
        # between the new origin and the old one.
        p[0] = p[0] - o[0]
        p[1] = p[1] - o[1]

        # Rotate the point.
        for i in range(3):  # i = 0, .., 2
            q[i] = self.dot_product(r[i], p)

        # Moves the point away from [0, 0] so that the new origin match its
        # real coordinates.
        q[0] = q[0] + o[0]
        q[1] = q[1] + o[1]

        return q

    def test(self, a, p, o, v, w):
        """
        Make a test.
        """

        print("dot product: ", tr.dot_product(v, w))

        print("counterclockwise rotation around X axes: ", tr.rxl(a, p, o))

        print("counterclockwise rotation around Y axes: ", tr.ryl(a, p, o))

        print("counterclockwise rotation around Y axes: ", tr.rzl(a, p, o))


if __name__ == '__main__':
    tr = Tr()

    # Test.
    v = [1, 1, 1]
    w = [2, 2, 2]
    p = [100, 100, 0]
    o = [100, 100, 0]
    a = 30
    tr.test(a, p, o, v, w)